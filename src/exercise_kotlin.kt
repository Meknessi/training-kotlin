//                      #8 class++ (open, : __() ,  constructor(use default param instead in some case), data, companion, object...)
/** Dans cet exercice interactif, vous allez devoir :
 *
 * - Creer une classe "User" possedant les proprietes "email", "password" et "urlImage" :
 * 	 * La classe User devra etre une classe "data"
 *   * La classe pourra etre instanciee, soit via son constructeur, soit via une methode
 *     de factory appelee "newInstance()"
 * 	 * Creer un objet pour cette classe et affichez son contenu
 * 	   dans la console de log.
 *
 * - Creer une classe "Button" possedant les proprietes "title" et "bgColor".
 * - Creer une classe "CircleButton" heritant de la classe "Button" et possedant
 * en plus la propriete "radius".
 * - Ces classes devront pouvoir etre construite avec chacune, toute ou aucune de
 * leurs proprietes
 * - Vous devrez egalement creer un objet de chacune de ces deux classes.
 *
 */
import utils.CircularButton
import utils.Button
import utils.User

fun main() {
    val myUser1:User = User.newInstance()
    val myUser2:User = User("test", urlImage = "a")
    println(myUser1)
    println(myUser2)
    val myButton = Button("Sun", 0x7F2020)
    println("title: ${myButton.title}\nbgColor: ${Integer.toHexString(myButton.bgColor)}\n")
    val myCircularButton = CircularButton("Earth", 0x2020FF, 32)
    println("title: ${myCircularButton.title}\nbgColor: ${Integer.toHexString(myCircularButton.bgColor)}\nradius: ${myCircularButton.radius}")
}

/*
//                      #7 Extensions train
import utils.toPlurat

fun main() {
    val str = "crayon"
    println(str)
    println(str.toPlurat())
}

/*
//                      #7 Function improvement
import utils.GOOGLE_URL
import utils.OPENCLASSROOM_URL
import utils.strIsNull

fun main() {
    val nonnull :String? = "je ne suis pas nul!"

    println(GOOGLE_URL)
    println(OPENCLASSROOM_URL)

    strIsNull(GOOGLE_URL)
    strIsNull(OPENCLASSROOM_URL)
    strIsNull(nonnull)
    //strIsNull(null)
}

/*
//                      #6 Exceptions training

private fun isUserOld(age: Int) = when {
            age > 100 -> throw Exception("Too old!")
            age > 65 -> true
            age > 0 -> false
            else -> throw Exception("Too young!")
}

fun main() {
    print(isUserOld(-77))
}

/*

//                      #5 Use of Any and casting training
private fun guessTheType(anyObj: Any) = when(anyObj){
    is String   -> "String"
    is Int      -> "Int"
    is List<*>  -> "List"
    is Boolean  -> "Boolean"
    is Array<*> -> "Array"
    else        -> "Unknown"
}
class Test(a:Int, b:Boolean)

fun main() {
    val test = Test(2, true)
    val listTest = arrayOf(77, "salut", listOf("salut", "les", "aminch"), arrayOf("salut", "les", "aminch"), true, test)

    val a:Any = true
    val b: Int? = a as? Int
    println("test a: $a, test b: $b")
    for(elem in listTest) println("The type is: ${guessTheType(elem)}.")

    /*
    println(guessTheType(77))
    println(guessTheType("salut"))
    println(guessTheType(listOf("salut", "les", "aminch")))
    println(guessTheType(arrayOf("salut", "les", "aminch")))
    println(guessTheType(true))
    println(guessTheType(test))
     */
}

/*
//                               #4 loop training

fun showArrayContent(array:Array<String>){
    for (elem in array) println(elem)
    println()
}

fun while20Items(){
    var j = 0
    while (j < 20) print("${j++} ")
    println()
}

fun main() {
    for (i in 0..30 step 2) print("$i ")
    println()
    for (i in 30 downTo 0 step 2) print("$i ")
    println()
    while20Items()
    showArrayContent(arrayOf("1st", "2nd", "3rd", "under 3rd"))
}

/*

//                               #3 Conditions & choices training

fun minOfMulTen(a:Int, b:Int) = if (a < b) a * 10 else b * 10

private enum class Colors(val clr:Byte) {
    RED(1),
    GREEN(2),
    BLUE(3),
    YELLOW(4),
    GREY(5),
    BLACK(6),
    WHITE(7),
    BROWN(8),
    ORANGE(9),
    PINK(10)
}

private fun printColorAndCode(colors: Colors) = when(colors) {
    Colors.RED -> println("Red color, color code = ${colors.clr}.")
    Colors.GREEN -> println("Green color, color code = ${colors.clr}.")
    Colors.BLUE -> println("Blue color, color code = ${colors.clr}.")
    else -> println("Another color : ${colors} code ${colors.clr}.")
}

fun main(args:Array<String>) {
    val a:Int = 244
    val b:Int = 232
    val colors = Colors.YELLOW

    println("\nConditional test with a multiplied result of $a or $b * 10:\n${minOfMulTen(a, b)}")
    printColorAndCode(colors)
}


/*
//                               #2 Class training

class Course(private val id:String, var title:String, val time:Int, var state:Boolean)

fun main(args:Array<String>) {
    val course = Course("Alph4837", "Title", 20, false)

    course.title = "Alphabet"
    course.state = true
    println("${course.title} ${course.time} ${course.state}")

}


/*
//                                #1 function training


fun getResult(a:Int, b:Int) = a + b
fun getUsernameUpperCase(username:String) = username.toUpperCase()
fun isUsernameOfTeacher(username: String) = username == "Phil" || username == "Ambroise"

fun main (args:Array<String>){
    val username = "Phil"

    println("Hello world!")
    println("Class size:  ${getResult(12, 30)}")
    print(getUsernameUpperCase("Class reps") + ": ")
    println(
            if (!isUsernameOfTeacher(username))
                "$username is'nt the teacher, don't worry."
            else
                "$username is the teacher, take place !"
    )
}
 */