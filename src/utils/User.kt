package utils

data class User(var email:String = "example@boitemail.fr",
                var password: String = "p@sSw0rD",
                var urlImage: String = "https://www.aufildescouleurs.com/17292-large_default/bamboo-r11821-8.jpg") {

    companion object {
        fun newInstance(email: String, password: String, urlImage: String): User = User(email, password, urlImage)
        fun newInstance(email: String, password: String): User = User(email, password)
        fun newInstance(): User = User()
    }
}