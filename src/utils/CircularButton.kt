package utils

class CircularButton: Button {

    public var radius: Byte = 50

    init {
        println("Circular button created")
    }

    constructor(title: String): super(title)
    constructor(title: String, bgColor: Int): super(title, bgColor)
    constructor(title: String, bgColor: Int, radius: Byte): super(title, bgColor){
        this.radius = radius
    }

}